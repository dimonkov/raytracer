#include "raytracer-exec/SphereLight.h"
#include "math-lib/vector3.h"

namespace raytracer
{
    SphereLight::SphereLight() : Origin(math::vector3(0,0,0)),
    Color(image::Color{1,1,1,1}), Intensity(250.0) { }
}
