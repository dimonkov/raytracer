#include <iostream>
#include <fstream>

#include "image-lib/Image.h"
#include "image-lib/Color.h"
#include "image-serialization/BmpSerialization.h"

#include "math-lib/shapes/Sphere.h"
#include "math-lib/shapes/Shape.h"
#include "math-lib/shapes/Plane.h"

#include "math-lib/ray.h"
#include "math-lib/vector2.h"
#include "image-lib/Color.h"
#include <math.h>

#include "raytracer-exec/Scene.h"

#include "raytracer-exec/CliParameters.h"
#include <memory>


int main(int argc, char** argv)
{
    auto cli = raytracer::CliParameters::ParseCliParameters(argc, argv);
    
    std::ifstream in;
    in.open(cli.scene_file_name.c_str());
    
    auto scene = raytracer::Scene::ParseSceneFromStream(in);
    
    in.close();
    
	image::Image img = scene.Render();

	std::ofstream out(cli.output_file_name.c_str(), std::ios::out | std::ios::binary);

	image::serialization::bmp::WriteAsBmpImage(out, img);

	out.close();

	return 0;
}
