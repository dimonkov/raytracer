#include <string>
#include <iostream>
#include <stdlib.h>
#include "raytracer-exec/CliParameters.h"

namespace raytracer
{
	CliParameters::CliParameters() { }

	CliParameters CliParameters::ParseCliParameters(int argc, char ** argv)
	{
		bool scene_file_name_specified = false;
        bool output_file_specified = false;
		CliParameters param;

		for (int i = 0; i < argc; i++)
		{
			std::string arg = std::string(argv[i]);
			if (arg == "-s" && i+1 < argc)
			{
				param.scene_file_name = std::string(argv[i + 1]);
				scene_file_name_specified = true;
			}
            if (arg == "-o" && i+1 < argc)
            {
                param.output_file_name = std::string(argv[i + 1]);
                output_file_specified = true;
            }
			if (arg == "-h")
			{
				std::cout << std::endl << "Options:" << std::endl;
				std::cout << "\"-h\" Prints this screen" << std::endl;
				std::cout << "\"-s\" <filename> Allows to specify the scene file name" << std::endl;
                std::cout << "\"-o\" <filename> Allows to specify the output file name" << std::endl;
				exit(EXIT_SUCCESS);
			}
		}

		if (!scene_file_name_specified)
		{
			std::cerr << "No scene file specified, please use -s <filename>!" << std::endl;
			exit(EXIT_FAILURE);
		}
        if (!output_file_specified)
        {
            std::cerr << "No output file specified, please use -o <filename>!" << std::endl;
            exit(EXIT_FAILURE);
        }
		return param;
	}
}
