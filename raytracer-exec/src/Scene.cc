#include "raytracer-exec/Scene.h"
#include <optional>
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <memory>
#include "math-lib/shapes/Shape.h"
#include "math-lib/shapes/Plane.h"
#include "math-lib/shapes/Sphere.h"
#include "raytracer-exec/GraphicsObject.h"

namespace raytracer
{
	image::Image Scene::Render()
	{
		 image::Image im = image::Image(m_width, m_height);

        int msaa_width = m_width * msaa;
        int msaa_height = m_height * msaa;
		 //Go through each pixel
		 for (int y = 0; y < m_height; y++)
		 {
			 for (int x = 0; x < m_width; x++)
			 {
                 image::Color col{0,0,0,0};
                 
                 for (int ym = 0; ym < msaa; ym++)
                 {
                     for(int xm = 0; xm < msaa; xm++)
                     {
                         col = col + TracePixel(x * msaa + xm, y * msaa + ym, msaa_width, msaa_height);
                     }
                 }
                 
                 col = col / ((float)msaa * msaa);
                 
                 im.SetPixelAt(x, y, col);
			 }
		 }
		 return im;
	}
	math::ray Scene::create_prime(int x, int y, int width, int height) const
	{
		double fov_rad = m_fov * M_PI / 180;

		//Calculate field of view adjustment
		double fov_adj = tan(fov_rad / 2.0);
		//Calculate aspect ratio
		double ar = (double)width / height;

		//Virtual camera sensor coordinates
		double sensor_x = ((((((double)x + 0.5) / (double)width) * 2.0 - 1.0) * ar)) * fov_adj;

		double sensor_y = (1.0 - (((double)y + 0.5) / (double)height) * 2.0);

		math::ray r;

		r.origin = math::vector3::zero();

		r.direction = math::vector3(sensor_x, sensor_y, -1.0).normalize();

		return r;
	}
    
    image::Color Scene::TracePixel(int x, int y, int width, int height) const
    {
        math::ray prime_ray = create_prime(x, y, width, height);
        
        auto trace_rez = Trace(prime_ray);
        
        if (trace_rez.obj != nullptr && trace_rez.Distance.has_value())
        {
            return GetColor(trace_rez.Distance.value(), prime_ray, *trace_rez.obj);
        }
        return BackgroundColor;
    }
    
    image::Color Scene::GetColor(double distance, math::ray ray,const GraphicsObject& obj) const
    {
        auto hit_point = ray.origin + (ray.direction * distance);
        auto surface_normal = obj.Shape->surface_normal(obj.Origin, hit_point);
        
        image::Color col{0,0,0,0};
        
        double light_reflected = obj.Color.A / M_PI;
        
        //Trace directional lights
        for (int i = 0; i < DirectionalLights.size(); i++)
        {
            auto direction_to_light = -DirectionalLights[i].Direction.normalize();
            
            math::ray shadow_ray;
            shadow_ray.origin = hit_point + (surface_normal * SHADOW_BIAS);
            shadow_ray.direction = direction_to_light;
            
            auto shadow_trace = Trace(shadow_ray);
            
            double light_intensity = shadow_trace.obj == nullptr ? DirectionalLights[i].Intensity : 0.0;
            
            double light_power = std::max(surface_normal.dot(direction_to_light), 0.0)
            * DirectionalLights[i].Intensity * light_intensity;
            
            col = col + obj.Color * DirectionalLights[i].Color * light_power * light_reflected;
            
        }
        
        //Trace sphere lights
        for (int i = 0; i < SphereLights.size(); i++)
        {
            auto vector_to_light = SphereLights[i].Origin - hit_point;
            
            //Squared distance to the light
            double r2 = vector_to_light.sqr_length();
            double dist = sqrt(r2);
            
            auto direction_to_light = vector_to_light / dist;
            
            math::ray shadow_ray;
            shadow_ray.origin = hit_point + (surface_normal * SHADOW_BIAS);
            shadow_ray.direction = direction_to_light;
            
            //Trace the shadow ray, to determine if light is obstructed
            auto shadow_trace = Trace(shadow_ray);
            
            bool in_light = !shadow_trace.Distance.has_value() ||
                                shadow_trace.Distance.value() > dist;
            
            double light_intensity = in_light ? SphereLights[i].Intensity / (4.0 * M_PI * r2) : 0.0;
            
            double light_power = std::max(surface_normal.dot(direction_to_light), 0.0)
            * SphereLights[i].Intensity * light_intensity;
            
            col = col + obj.Color * SphereLights[i].Color * light_power * light_reflected;
        }
        
        
        return col.Clamp();
    }
    
    raytracer::RaytracingResult Scene::Trace(const math::ray& ray) const
    {
        GraphicsObject* closest_object = nullptr;
        std::optional<double> closest;
        
        //Determine the closest object
        for (int i = 0; i < graphics_objects.size(); i++)
        {
            GraphicsObject* obj = graphics_objects[i].get();
            math::shapes::Shape* shape = obj->Shape.get();
            
            //Raycast into that object
            std::optional<double> rez = shape -> intersect(obj->Origin, ray);
            
            //Raycast hit the object, potentially it's the one we are looking for
            if (rez.has_value())
            {
                if (!closest.has_value() || closest.value() > rez.value())
                {
                    //Save the closest object for later, to get the pixel color
                    closest = rez;
                    closest_object = obj;
                }
            }
        }
        
        return RaytracingResult{closest_object, closest};
    }
    
    const Scene Scene::ParseSceneFromStream(std::istream& input)
    {
        int width = 0, height = 0, multisample = 1;
        float fov = 90;
        
        
        
        input >> width >> height >> fov >> multisample;
        
        image::Color bg;
        input >> bg.R >> bg.G >> bg.B >> bg.A;
        
        Scene sc(width, height, fov);
        sc.BackgroundColor = bg;
        sc.msaa = multisample;
        
        int sphere_count = 0;
        
        input >> sphere_count;
        
        //Spheres
        for(int i = 0;i < sphere_count;i++)
        {
            math::vector3 orig;
            double radius;
            image::Color col;
            
            input >> orig.X >> orig.Y >> orig.Z
                >> radius
                >> col.R >> col.G >> col.B >> col.A;
            
            auto shape = std::make_unique<math::shapes::Sphere>();
            shape->Radius = radius;
            
            auto obj = std::make_unique<GraphicsObject>();
            obj->Origin = orig;
            obj->Shape = std::move(shape);
            obj->Color = col;
            
            sc.graphics_objects.push_back(std::move(obj));
        }
        
        //------------------
        int plane_count = 0;
        input >> plane_count;
        
        //Planes
        for(int i = 0;i < plane_count;i++)
        {
            math::vector3 orig;
            math::vector3 norm;
            image::Color col;
            
            input >> orig.X >> orig.Y >> orig.Z
            >> norm.X >> norm.Y >> norm.Z
            >> col.R >> col.G >> col.B >> col.A;
            
            auto shape = std::make_unique<math::shapes::Plane>();
            shape->Normal = norm;
            
            auto obj = std::make_unique<GraphicsObject>();
            obj->Origin = orig;
            obj->Shape = std::move(shape);
            obj->Color = col;
            
            sc.graphics_objects.push_back(std::move(obj));
        }
        
        //----------------
        int dir_ligth_count = 0;
        input >> dir_ligth_count;
        
        //Directional lights
        for(int i = 0;i < dir_ligth_count;i++)
        {
            math::vector3 dir;
            image::Color col;
            double intensity;
            
            input >> dir.X >> dir.Y >> dir.Z
            >> intensity
            >> col.R >> col.G >> col.B >> col.A;
            
            
            raytracer::DirectionalLight dir_light;
            dir_light.Direction = dir;
            dir_light.Color = col;
            dir_light.Intensity = intensity;
            
            sc.DirectionalLights.push_back(dir_light);
        }
        
        
        //---------------
        int sphere_light_count = 0;
        input >> sphere_light_count;
        
        //Sphere lights
        for(int i = 0;i < sphere_light_count;i++)
        {
            math::vector3 origin;
            image::Color col;
            double intensity;
            
            input >> origin.X >> origin.Y >> origin.Z
            >> intensity
            >> col.R >> col.G >> col.B >> col.A;
            
            raytracer::SphereLight sph;
            sph.Origin = origin;
            sph.Color = col;
            sph.Intensity = intensity;
            
            sc.SphereLights.push_back(sph);
        }
        
        return sc;
    }
}
