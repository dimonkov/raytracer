#include "raytracer-exec/DirectionalLight.h"
#include "math-lib/vector3.h"

namespace raytracer
{
    DirectionalLight::DirectionalLight() : Direction(math::vector3(0,-1,-1)),
    Color(image::Color{1,1,1,1}), Intensity(1.5) { }
}
