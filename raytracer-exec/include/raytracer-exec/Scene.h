#ifndef RAYTRACER_SCENE_H_  
#define RAYTRACER_SCENE_H_

#include <vector>
#include <istream>

#include "raytracer-exec/GraphicsObject.h"
#include "image-lib/Image.h"
#include "raytracer-exec/RaytracingResult.h"
#include "raytracer-exec/DirectionalLight.h"
#include "raytracer-exec/SphereLight.h"

namespace raytracer
{
	class Scene
	{
	public:
		std::vector<std::unique_ptr<GraphicsObject> > graphics_objects;

		Scene(unsigned int width,unsigned int height, float fow) : m_width(width), m_height(height), m_fov(fow) { }

		image::Image Render();

		image::Color BackgroundColor;

        std::vector<raytracer::DirectionalLight> DirectionalLights;
        
        std::vector<raytracer::SphereLight> SphereLights;
        
		math::ray create_prime(int x, int y, int width, int height) const;
        
        image::Color TracePixel(int x, int y, int width, int height) const;
        
        image::Color GetColor(double distance, math::ray ray, const GraphicsObject& obj) const;
        
        raytracer::RaytracingResult Trace(const math::ray& ) const;
        
        
        static const Scene ParseSceneFromStream(std::istream& input);

        int msaa = 1;
	private:
		unsigned int m_width, m_height;
		float m_fov;
        const double SHADOW_BIAS = 1e-13;

	};
}

#endif // !RAYTRACER_SCENE_H_
