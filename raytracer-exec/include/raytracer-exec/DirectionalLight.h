#ifndef RAYTRACER_DIRECTIONAL_LIGHT_H_
#define RAYTRACER_DIRECTIONAL_LIGHT_H_

#include "image-lib/Color.h"
#include "math-lib/vector3.h"

namespace raytracer
{
    class DirectionalLight
    {
    public:
        DirectionalLight();
        
        math::vector3 Direction;
        
        image::Color Color;
        
        double Intensity;
    };
}

#endif //!RAYTRACER_DIRECTIONAL_LIGHT_H_
