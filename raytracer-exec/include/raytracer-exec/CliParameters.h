#ifndef RAYTRACER_CLI_PARAMETERS_H_
#define RAYTRACER_CLI_PARAMETERS_H_
#include <string>

namespace raytracer
{
	class CliParameters
	{
	public:
		std::string scene_file_name;
        std::string output_file_name;

		CliParameters();

		static CliParameters ParseCliParameters(int argc, char** argv);
	};
}

#endif // !RAYTRACER_CLI_PARAMETERS_H_
