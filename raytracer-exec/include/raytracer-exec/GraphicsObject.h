#ifndef RAYTRACER_GRAPHICS_OBJECT_H_  
#define RAYTRACER_GRAPHICS_OBJECT_H_

#include <memory>

#include "math-lib/vector3.h"
#include "math-lib/shapes/Shape.h"
#include "image-lib/Color.h"

namespace raytracer
{
	class GraphicsObject
	{
		public:
		math::vector3 Origin;

		std::unique_ptr<math::shapes::Shape> Shape;

		image::Color Color;
	};
}

#endif // !RAYTRACER_GRAPHICS_OBJECT_H_
