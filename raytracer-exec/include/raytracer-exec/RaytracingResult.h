#ifndef RAYTRACER_RAYTRACING_RESULT_H_
#define RAYTRACER_RAYTRACING_RESULT_H_

#include "raytracer-exec/GraphicsObject.h"
#include <optional>

namespace raytracer
{
    struct RaytracingResult
    {
        raytracer::GraphicsObject* obj;
        std::optional<double> Distance;
    };
}

#endif //!RAYTRACER_RAYTRACING_RESULT_H_
