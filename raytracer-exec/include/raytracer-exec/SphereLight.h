#ifndef RAYTRACER_SPHERE_LIGHT_H_
#define RAYTRACER_SPHERE_LIGHT_H_

#include "image-lib/Color.h"
#include "math-lib/vector3.h"

namespace raytracer
{
    class SphereLight
    {
    public:
        SphereLight();
        
        math::vector3 Origin;
        
        image::Color Color;
        
        double Intensity;
    };
}

#endif //!RAYTRACER_SPHERE_LIGHT_H_
